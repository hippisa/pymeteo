""" Modulo per gli oggetti di configurazione, condivisi """
import os

# opeweathermap api key
KEY = '50ccfe564e0cfa58bcde81c093b6d557'


""" Logging configurations """
default_logger = "default"
debug_logger = "debug"
if os.getenv("LOGLVL") == debug_logger:
    default_logger = debug_logger

home = os.getenv("HOME")
xdg_data = os.getenv("XDG_DATA_HOME", "{}/.local/share".format(home))

default_logdir = "{}/pymeteo".format(xdg_data)
logdir = os.getenv("LOGDIR", default_logdir)

default_logfilename = "pymeteo.log"
logfilename = os.getenv("LOGFILE", default_logfilename)

""" Cities File configurations """
default_cityfile = "{}/local/git/pymeteo/bin/city.list.json".format(home)
cityfile = os.getenv("CITYFILE", default_cityfile)
