import logging.config
import pathlib

import conf

pathlib.Path(conf.logdir).mkdir(exist_ok=True)
#    https://stackoverflow.com/questions/273192/how-can-i-create-a-directory-if-it-does-not-exist

logging_setup = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'logfile': {
            'format': (
                "%(asctime)s [%(levelname)s] %(threadName)s-%(module)s:"
                "%(lineno)d-%(funcName)s: %(message)s")
            },
        'console': {
            'format': "%(levelname)s: %(message)s"
            },
        'console-verbose': {
            'format': (
                "%(asctime)s [%(levelname)s] %(threadName)s-%(module)s:"
                "%(lineno)d-%(funcName)s: %(message)s")
            },
        },

    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
            },

        'stderr': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stderr',
            'formatter': 'console-verbose',
            'level': 'ERROR',
            },

        'stdout': {
            'class': 'logging.StreamHandler',
            'stream': 'ext://sys.stdout',
            'formatter': 'console-verbose',
            'level': 'DEBUG',
            },

        'logfile': {
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '{}/{}'.format(conf.logdir, conf.logfilename),
            'maxBytes': 10485760,           # 10MB
            'backupCount': 2,
            'encoding': 'utf8',
            'formatter': 'logfile',
            'level': 'DEBUG',
            },

        },

    'root': {
        # è il logger di default e il più alto nella gerarchia, null utile
        # per i library developers
        'handlers': ['null']
        },

    'loggers': {
        "default": {
            'level': 'INFO',
            'handlers': ['stdout', 'logfile'],
            'propagate': False
            },

        "debug": {
            'level': 'DEBUG',
            'handlers': ['stdout', 'logfile'],
            'propagate': False
            #   passa il messaggio ai logger di livello superiore nella
            #   gerarchia
            #   "root.parent.child"
            },

        },
    }

logging.config.dictConfig(logging_setup)


def get_loggers(debug, timing_verbose):
    """ Loggers initialization for modules """
    if debug:
        logger = logging.getLogger(conf.debug_logger)
    else:
        logger = logging.getLogger(conf.default_logger)
    if timing_verbose:
        timelogger = logging.getLogger(conf.debug_logger)
    else:
        timelogger = logging.getLogger(conf.default_logger)
    return logger, timelogger
